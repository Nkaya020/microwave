﻿
namespace Microwave
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartButton = new System.Windows.Forms.Button();
            this.OpenButton = new System.Windows.Forms.Button();
            this.lampImage = new System.Windows.Forms.PictureBox();
            this.doorPicture = new System.Windows.Forms.PictureBox();
            this.timeLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mealComboBox = new System.Windows.Forms.ComboBox();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.buttonMinus = new System.Windows.Forms.Button();
            this.mealTextBox = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lampImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doorPicture)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(646, 321);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(142, 59);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(647, 386);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(141, 51);
            this.OpenButton.TabIndex = 2;
            this.OpenButton.Text = "Open";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // lampImage
            // 
            this.lampImage.BackColor = System.Drawing.Color.Transparent;
            this.lampImage.Image = global::Microwave.Properties.Resources.off;
            this.lampImage.Location = new System.Drawing.Point(250, 12);
            this.lampImage.Name = "lampImage";
            this.lampImage.Size = new System.Drawing.Size(147, 210);
            this.lampImage.TabIndex = 3;
            this.lampImage.TabStop = false;
            // 
            // doorPicture
            // 
            this.doorPicture.Image = global::Microwave.Properties.Resources.closed;
            this.doorPicture.Location = new System.Drawing.Point(12, 12);
            this.doorPicture.Name = "doorPicture";
            this.doorPicture.Size = new System.Drawing.Size(628, 426);
            this.doorPicture.TabIndex = 1;
            this.doorPicture.TabStop = false;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(19, 14);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(94, 31);
            this.timeLabel.TabIndex = 4;
            this.timeLabel.Text = "00:00";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.timeLabel);
            this.panel1.Location = new System.Drawing.Point(647, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(141, 59);
            this.panel1.TabIndex = 5;
            // 
            // mealComboBox
            // 
            this.mealComboBox.FormattingEnabled = true;
            this.mealComboBox.Location = new System.Drawing.Point(4, 58);
            this.mealComboBox.Name = "mealComboBox";
            this.mealComboBox.Size = new System.Drawing.Size(111, 21);
            this.mealComboBox.TabIndex = 6;
            // 
            // buttonPlus
            // 
            this.buttonPlus.Location = new System.Drawing.Point(118, 32);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(18, 20);
            this.buttonPlus.TabIndex = 7;
            this.buttonPlus.Text = "+";
            this.buttonPlus.UseVisualStyleBackColor = true;
            this.buttonPlus.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonMinus
            // 
            this.buttonMinus.Location = new System.Drawing.Point(119, 56);
            this.buttonMinus.Name = "buttonMinus";
            this.buttonMinus.Size = new System.Drawing.Size(17, 23);
            this.buttonMinus.TabIndex = 8;
            this.buttonMinus.Text = "-";
            this.buttonMinus.UseVisualStyleBackColor = true;
            this.buttonMinus.Click += new System.EventHandler(this.buttonMinus_Click);
            // 
            // mealTextBox
            // 
            this.mealTextBox.Location = new System.Drawing.Point(3, 32);
            this.mealTextBox.Name = "mealTextBox";
            this.mealTextBox.Size = new System.Drawing.Size(112, 20);
            this.mealTextBox.TabIndex = 9;
            this.mealTextBox.Text = "Gerechtnaam";
            this.mealTextBox.UseWaitCursor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.buttonMinus);
            this.panel2.Controls.Add(this.mealTextBox);
            this.panel2.Controls.Add(this.mealComboBox);
            this.panel2.Controls.Add(this.buttonPlus);
            this.panel2.Location = new System.Drawing.Point(647, 230);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(141, 85);
            this.panel2.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "Gerechten";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lampImage);
            this.Controls.Add(this.OpenButton);
            this.Controls.Add(this.doorPicture);
            this.Controls.Add(this.StartButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lampImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doorPicture)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.PictureBox doorPicture;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.PictureBox lampImage;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox mealComboBox;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button buttonMinus;
        private System.Windows.Forms.TextBox mealTextBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
    }
}

