﻿using Microwave.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Microwave
{
    public partial class Form1 : Form
    {
        private bool DoorOpen = false;
        private int Time = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Time += 30;
            timeLabel.Text = $"{(Time / 60).ToString("0#")}:{(Time % 60).ToString("0#")}";
            if(Time > 0)
            {
                if(!DoorOpen)
                {
                    lampImage.Image = (Image)Resources.ResourceManager.GetObject("on");
                }
            } else
            {

                lampImage.Image = (Image)Resources.ResourceManager.GetObject("off");
            }
        }


        private void OpenButton_Click(object sender, EventArgs e)
        {
            DoorOpen = !DoorOpen;
            switch(DoorOpen)
            {
                case true:
                    doorPicture.Image = (Image)Resources.ResourceManager.GetObject("open");
                    lampImage.Image = (Image)Resources.ResourceManager.GetObject("off");
                    OpenButton.Text = "Close";
                    break;
                case false:
                    doorPicture.Image = (Image)Resources.ResourceManager.GetObject("closed");
                    if (Time > 0)
                    {
                        lampImage.Image = (Image)Resources.ResourceManager.GetObject("on");
                    }
                    OpenButton.Text = "Open";
                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += new EventHandler((obj, args) =>
            {
                if (Time > 0 && !DoorOpen) Time--;
                timeLabel.Text = $"{(Time / 60).ToString("0#")}:{(Time % 60).ToString("0#")}";
            });
            timer.Start();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if(mealTextBox.Text != "" && !mealComboBox.Items.Contains(mealTextBox.Text))
            {
                mealComboBox.Items.Add(mealTextBox.Text);
                mealComboBox.SelectedItem = mealTextBox.Text;
            }
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            mealComboBox.Items.Remove(mealComboBox.SelectedItem);
            mealComboBox.SelectedItem = "";
            mealComboBox.Text = "";
        }
    }
}
